use std::iter::zip;
use std::{io, thread};
use std::time::{Duration, Instant};
use gpio::GpioOut;
use gpio::sysfs::SysFsGpioOutput as GpioOutput;

const COL_ADDR: [u16; 3] = [26, 19, 13];
const COL_OE_LOW: u16 = 6;
const SR_DATA: u16 = 5;
const SR_SHIFT_CLK: u16 = 0;
const SR_STORE_CLK: u16 = 11;

fn get_bits(byte: u8) -> [bool; 8] {
    let mut bits = [false; 8];

    for bit in 0..8 {
        if (byte & (1 << bit)) > 0 {
            bits[bit] = true;
        }
    }

    bits
}


const SHIFT_DELAY: Duration = Duration::from_micros(1);


struct ShiftRegister {
    data_in: GpioOutput,
    shift_clock: GpioOutput,
    show_clock: GpioOutput
}


trait Pulse {
    fn pulse(&mut self, time_high: Duration, time_low: Duration) -> io::Result<()>;
}


impl Pulse for GpioOutput {
    fn pulse(&mut self, time_high: Duration, time_low: Duration) -> io::Result<()> {
        self.set_high()?;
        thread::sleep(time_high);
        self.set_low()?;
        thread::sleep(time_low);
        Ok(())
    }
}


impl ShiftRegister {
    fn shift_byte(&mut self, byte: u8) -> io::Result<()> {
        for bit in get_bits(byte) {

            thread::sleep(SHIFT_DELAY);
            self.data_in.set_value(bit)?;

            self.shift_clock.pulse(SHIFT_DELAY, SHIFT_DELAY)?;
        }

        Ok(())
    }
}


fn main() -> io::Result<()> {
    let image_data: Vec<u8> = include_bytes!("hello_world.data")
        .chunks(8)
        .map(|bits| {
            let mut byte = 0u8;

            for (idx, bit) in bits.iter().enumerate() {
                byte |= bit << idx;
            }

            byte
        })
        .collect();



    let mut col_oe_pin = GpioOutput::open(COL_OE_LOW)?;
    let mut col: u8 = 0;

    let mut addr_pins: Vec<GpioOutput> = COL_ADDR.iter().map(|pin_nr| {
        GpioOutput::open(*pin_nr)
    }).collect::<io::Result<Vec<GpioOutput>>>()?;

    let mut shift_register = ShiftRegister {
        data_in: GpioOutput::open(SR_DATA)?,
        shift_clock: GpioOutput::open(SR_SHIFT_CLK)?,
        show_clock: GpioOutput::open(SR_STORE_CLK)?
    };

    let mut offset: usize = 0;
    let mut last_update = Instant::now();

    loop {
        let idx = (offset + col as usize) % image_data.len();
        shift_register.shift_byte(image_data[idx])?;

        col_oe_pin.set_high()?;

        for (val, pin) in zip(get_bits(col), addr_pins.iter_mut()) {
            pin.set_value(val)?;
        }

        shift_register.show_clock.pulse(SHIFT_DELAY, SHIFT_DELAY)?;
        col_oe_pin.set_low()?;

        thread::sleep(Duration::from_micros(10));

        col = (col + 1) % 8;

        if col == 0 && last_update.elapsed() > Duration::from_millis(120) {
            offset += 1;
            last_update = Instant::now();
        }
    }
}
